import Axios from 'axios';

var Get = (url) => {
    return new Promise((resolve, reject) => {
        Axios.get(url).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err);
        })
    })
}

var Put = (url, data) => {
    return new Promise((resolve, reject) => {
        Axios.put(url, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err);
        })
    })
}

var Post = (url, data) => {
    return new Promise((resolve, reject) => {
        Axios.post(url, data).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err);
        })
    })
}

var Delete = (url) => {
    return new Promise((resolve, reject) => {
        Axios.delete(url).then(res => {
            resolve(res)
        }).catch(err => {
            reject(err);
        })
    })
}

export default {
    GET: Get,
    PUT: Put,
    POST: Post,
    DELETE: Delete
}