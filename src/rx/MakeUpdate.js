import {Subject} from 'rxjs';
import api from '../service/api';

const MakeUpdate = new Subject();

MakeUpdate.subscribe(val => {
    api.PUT(val.url, val.data).then(res => {
        console.log(res);
        val.callback();
    });
})

export default MakeUpdate;