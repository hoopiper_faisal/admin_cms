import {Subject} from 'rxjs';
import api from '../service/api';

const MakeDelete = new Subject();

MakeDelete.subscribe(val => {
    api.DELETE(val.url).then(res => {
        console.log(res);
        val.callback();
    });
})

export default MakeDelete;