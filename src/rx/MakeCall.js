import {Subject} from 'rxjs';
import api from '../service/api';

const MakeCall = new Subject();

MakeCall.subscribe(val => {
    api.GET(val.url).then(res => {
        console.log(res);
        val.callback(res.data);
    });
})

export default MakeCall;