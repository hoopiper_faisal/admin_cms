//import Employee from './employee_list'

var dash = [
    {
        path: "/employee",
        name: "Employee",
        component: './employee_list',
        icon: 'business_badge',
        api_get: 'http://dummy.restapiexample.com/api/v1/employees',
        api_update: (id) => {return `http://dummy.restapiexample.com/api/v1/update/${id}`;},
        api_delete: (id) => {return `http://dummy.restapiexample.com/api/v1/delete/${id}`;}
    }
]

export default dash;