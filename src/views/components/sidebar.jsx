import React from "react";
import { NavLink } from "react-router-dom";
import { Nav } from "reactstrap";
import Logo from "./logo-white.svg";


class Sidebar extends React.Component {

    ActiveRoute = (routeName) => {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "Active" : "";
    }

    render() {
        return (
            <div className="sidebar" data-color={this.props.backgroundColor}>
                <div className="logo">
                    <a href="https://hoopiper.com" className="simple-text logo-mini" target="_blank" >
                        <div className="logo-img">
                            <img src={Logo} alt="react-logo" />
                        </div>
                    </a>
                
                <a href="https://hoopiper.com" className="simple-text logo-normal" target="_blank" >
                    Hoopiper Team
                </a>
                </div>
                <div className="sidebar-wrapper" ref="sidebar">
                    <Nav>
                        {this.props.routes.map((val, i) => {
                            return (
                                <li className={this.ActiveRoute(val.path)}>
                                    <NavLink key={'nav_${i}'} to={val.path} className="nav-link" activeClassName="active">
                                        <i className={"now-ui-icons " + val.icon} />
                                        <p>{val.name}</p>
                                    </NavLink>
                                </li>
                            )
                        })}
                    </Nav>
                </div>
            </div>
        )
    }
}

export default Sidebar;
