/*eslint-disable*/

import React, {Component} from "react";
import { Container } from "reactstrap";

class Footer extends Component {
  render() {
    return (
      <footer className={"footer" + (this.props.default ? " footer-default" : "")}>
        <Container fluid={this.props.fluid ? true : false}>
          <div className="copyright"> &copy; {1900 + new Date().getYear()}, Designed by{" "}
            <a href="https://hoopiper.com/" target="_blank" rel="noopener noreferrer">
              Hoopiper
            </a>
            . Coded by{" "}
            <a href="https://hoopiper.com/"
              target="_blank" rel="noopener noreferrer" >
              Hoopiper Team
            </a>
            .
          </div>
        </Container>
      </footer>
    );
  }
}

export default Footer;
