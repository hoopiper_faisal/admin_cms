import React, {Component} from 'react';
import {Subject} from 'rxjs';
import {Redirect} from 'react-router-dom';

const InputStream = new Subject();

const ls = {
    margin: "0 auto",
    marginTop: "70px",
    maxWidth: "200px",
    padding: "40px 40px",
    background: "#fff"
};

class Login extends Component {

    state = {
        username: '',
        password: '',
        redirect: false
    }

    componentDidMount() {
        InputStream.subscribe(val => {
            console.log(val);
            switch(val.key) {
                case "username":
                    this.setState({username: val.value});
                    break;

                case "password":
                    this.setState({password: val.value});
                    break;

                default:
                    break;
            }
        });
    }

    componentWillUnmount() {
        InputStream.unsubscribe();
    }

    
    render() {
        if(this.state.redirect) {
            return <Redirect to='/'/>
        }

        return (
            <div className="Login" style={ls}>
                <form onSubmit={() => {this.setState({redirect: true})}}>
                <label>User Name</label>
                <input type="text" data-test="username" value={this.state.username} onChange={(e) => {InputStream.next({key: "username", value: e.target.value})}} />
                <p>&nbsp;</p>
                <label>Password</label>
                <input type="password" data-test="password" value={this.state.password} onChange={(e) => {InputStream.next({key: "password", value: e.target.value})}} />
                <p>&nbsp;</p>
                <input type="submit" value="Log In" data-test="submit" />
                </form>
            </div>
        )
    }

}

export default Login;