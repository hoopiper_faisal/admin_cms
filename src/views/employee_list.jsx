import React, {Component} from 'react';
import MakeCall from '../rx/MakeCall';
import MakeUpdate from '../rx/MakeUpdate';
import MakeDelete from '../rx/MakeDelete';

import {
    Card,
    CardBody,
    CardHeader,
    CardTitle,
    Table,
    Row,
    Col,
    Input,
    Button, 
    Spinner
  } from "reactstrap";

import PanelHeader from './components/panel_header';
import {isArray} from 'util';

let col = ["ID", "Name", "Salary", "Age", ""]

export default class EmployeeList extends Component {

    state = {
        data: [],
        loading: true,
        edit: -1
    }

    input = {
        name: '',
        age: '',
        salary: ''
    }

    componentDidMount() {
        MakeCall.next({url: this.props.api_get,
                callback: (value) => {this.setState({data: value, loading: false})}
        });
    }

    handleUpdate = (id, idx) => {
        MakeUpdate.next({url: this.props.api_update(id),
            data: this.input,
            callback: () => {
                let val = this.state.data;
                val[idx].employee_name = this.input.name;
                val[idx].employee_age = this.input.age;
                val[idx].employee_salary = this.input.salary;
                this.setState({data: val, edit: -1}, () => {
                    this.input = {
                        name: '',
                        age: '',
                        salary: ''
                    }
                });
            }
        });
    }

    handleDelete = (id) => {
        MakeDelete.next({url: this.props.api_delete(id),
                callback: () => {  let val = this.state.data.filter(v => v.id !== id);
                                        this.setState({data: val});}
        });
    }

    render() {
        console.log(this.props);
        let data = this.state.data.map((v, i) => {
            if(i === this.state.edit) {
                return {
                    data: [
                        v.id,
                        <Input defaultValue={v.employee_name} onChange={(e) => {this.input.name = e.target.value}} />,
                        <Input defaultValue={v.employee_salary} onChange={(e) => {this.input.salary = e.target.value}}/>,
                        <Input defaultValue={v.employee_age} onChange={(e) => {this.input.age = e.target.value}}/>,
                        <div>
                            <Button onClick={() => this.handleUpdate(v.id, i)} color="info">Save</Button>
                            <Button color="danger" onClick={() => this.setState({edit: -1})}>Cancel</Button>
                        </div>
                    ]
                }
            }
            return {
                data: [
                    v.id,
                    v.employee_name,
                    v.employee_salary,
                    v.employee_age,
                    <div>
                        <Button color="info" onClick={() => this.setState({edit: i}, () => { this.input = {name: v.employee_name, age: v.employee_age, salary: v.employee_salary}})}>Edit</Button>
                        <Button color="danger" onClick={() => {this.handleDelete(v.id)}}>Delete</Button>
                    </div>
                ]
            }
        });

        return (
            <>
                <PanelHeader size="sm" />
                <div className="content">
                    <Row>
                        <Col xs={12}>
                            <Card>
                                <CardHeader>
                                    <CardTitle tag="h4">Employee Lists</CardTitle>
                                </CardHeader>
                                    <CardBody> 
                                        {this.state.loading ? <Spinner /> : <Table responsive>
                                            <thead className="text-primary">
                                                <tr>
                                                    {col.map((v, i) => {
                                                        return <th key={i}>{v}</th>;
                                                    })}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {data.map((v, i) => {
                                                    return <tr key={i}>
                                                        {isArray(v.data) ? v.data.map((val, idx) => {
                                                            return <td key={idx}>{val}</td>;
                                                        }) : null}
                                                    </tr>
                                                })}
                                            </tbody>
                                        </Table>}
                                    </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </>
        )
    }
}