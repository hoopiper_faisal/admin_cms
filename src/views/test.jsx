
import React, {Component} from 'react';
import MakeCall from '../rx/MakeCall'

export default class Test extends Component {

    state = {
        data: []
    }

    componentDidMount() {
        MakeCall.next({url: 'http://dummy.restapiexample.com/api/v1/employees',
                callback: (value) => {this.setState({data: value})}
        });
    }

    render() {
        return (<p>{JSON.stringify(this.state)}</p>)
    }
}