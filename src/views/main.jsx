import React, {Component, Suspense, lazy} from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import Footer from './components/footer';
import Nav from './components/nav';
import Sidebar from './components/sidebar';
import Routes from './routes';

var Dynamic = (path) => {
    const Page = lazy(() => (import(`${path}`)));
    return Page
}

class Main extends Component {

    render() {
        console.log(this.props);
        let Rts = Routes.map((v, i) => {
            let Page = Dynamic(v.component);
            return <Route key={i} path={v.path} render={() => <Suspense fallback={null}><Page api_get={v.api_get} api_update={v.api_update} api_delete={v.api_delete}/></Suspense>} />
        });
        return (
            <div className="wrapper">
                <Sidebar location={this.props.details.location} routes={Routes}/>
                <div className="main-panel" ref={this.mainPanel}>
                    <Nav />
                    <Switch>
                        {Rts.map(v => {return v})}
                        <Redirect to={Routes[0].path} />
                    </Switch>
                    <Footer fluid />
                </div>
            </div>
        )
    }
}

export default Main;
