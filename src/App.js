import React from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Main from './views/main';
import Login from './views/login';

class App extends React.Component {

  render() {
    return (
      <Provider store={this.props.store}>
        <BrowserRouter>
          <Switch>
            <Route path='/' render={(props) => <Main details={props} />}/>
            <Route path='/login' render={(props) => <Login details={props} />} />
          </Switch>
        </BrowserRouter>
      </Provider>
    )
  }
}

export default App;
