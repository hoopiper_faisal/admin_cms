import {createStore} from 'redux';
import root from './root_reducer';

export default createStore(root);