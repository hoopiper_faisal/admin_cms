import constant from '../constants/app';

const app = {
    login: false
}

export default function appReducer(state = app, { type, payload }) {
    switch(type) {
        case constant.LOGIN_PARAMETER:
            return {...state, login: payload}
        default:
            return state
    }
}